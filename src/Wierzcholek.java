import java.util.*;
import java.lang.*;
import java.io.*;




public class Wierzcholek 
{
	
  
	int poczatek;
	int koniec;
	int waga;
	
   
    public Wierzcholek(int poczatek2, int koniec2, int waga2) {
    	
        this.waga = waga2;
        this.koniec = koniec2;
        this.poczatek = poczatek2;
	}

}

class Ideone
{
	
	int n, m, k, v;
	int infinity = 99999;
	
	int[] d; // koszty dojscia
	int[] p; // poprzednik
	
	int[][] sasiedztwa;
	Wierzcholek[] wierzcholki;
	boolean[] przetworzone;
	
	int i;
	public void inicjalizacja() throws FileNotFoundException
	{
		File plik = new File("out.txt");
		Scanner odczyt = new Scanner(new File("in.txt"));
		
		 n = odczyt.nextInt();
		 m = odczyt.nextInt();
		k = odczyt.nextInt();
		 v = 0;	
		 
		d = new int[n];// koszty dojscia
		p = new int[n]; // poprzednik
		przetworzone = new boolean[n];
		wierzcholki = new Wierzcholek[m];
		sasiedztwa = new int[m][m];
				
		for(i = 0; i < n; i++)
		{
			d[i] = infinity;
			p[i] = -1;
			przetworzone[i] = false;
		}
		
		for(i = 0; i < m; i++)
		{
			
			int poczatek = odczyt.nextInt();
			int koniec = odczyt.nextInt();
			int waga = odczyt.nextInt();
			
			wierzcholki[i] = new Wierzcholek(poczatek, koniec, waga);
			
			sasiedztwa[poczatek][koniec] = 1;
			sasiedztwa[koniec][poczatek] = 1;
			
		}
		
		
		
	}


    public void wyznaczanie() 
    {
    d[0] = 0;
    p[0] = 0;
    int min_koszt = 0;
    int v_nr = -1; // indekst wierzcholka i minimalnym koszcie dojscia
   
    
    while(przetworzone[n-1] != true){
    	min_koszt = 99999;
    for(int licznik = 0; licznik < n; licznik++)
    {
 	
    	if(d[licznik] <= min_koszt && przetworzone[licznik] == false){
    		min_koszt = d[licznik];
    		v_nr = licznik;
    		break;
    		
    	}

    }
    
   przetworzone[v_nr] = true;

    //przeszukujemy s�siad�w wierzcho�ka
   
   int indeks_sasiada = -1;
   int koszt_przejscia = -1;
   for(i = 0; i < n+1; i++)
   {
	   if(sasiedztwa[v_nr+1][i] == 1 && przetworzone[i-1] == false)
	   {
		   indeks_sasiada = i-1;
		   for(int j = 0; j < m; j++)
		   {
			   if(wierzcholki[j].koniec-1 == indeks_sasiada && wierzcholki[j].poczatek-1 == v_nr){
				   koszt_przejscia = wierzcholki[j].waga;
			   break;}
		   }
		   
		   if(d[indeks_sasiada] > d[v_nr] + koszt_przejscia)
		   {
			   d[indeks_sasiada] = d[v_nr] + koszt_przejscia;
			   p[indeks_sasiada] = v_nr;
		   }
	   }
   }
   
   przetworzone[v_nr] = true;
    	
    }

    }
  public void wypisz()
  {
	 
	  System.out.println("");
	  System.out.println("Najszybsza droga: ");
	  int pom = p[n-1]+1, pom2 = 0;
	System.out.println(n);
	System.out.println(p[n-1]+1);
	  while(pom != 1)
	  {
		  
		  System.out.println(p[pom-1]+1 +" ");
		  pom = p[pom-1]+1;
		 
	  }
  }
  
  	 

    
	public static void main (String[] args) throws FileNotFoundException 
	{
		Ideone odczyt = new Ideone();
		odczyt.inicjalizacja();
		odczyt.wyznaczanie();
		odczyt.wypisz();
		
		
	}
}
